package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os/exec"
)

const (
	command          = "osascript"
	runCommand       = "display notification \"%s\" with title \"%s\""
	phrase           = "Mamma in arrivo\nSpegni tutto e studia"
	portRemoteServer = "8080"
)

func main() {

	flag.Parse()

	if len(flag.Args()) < 1 {
		log.Fatalln("I need the server IP!!", flag.Args())
	}

	// Login to the robot
	http.Get(fmt.Sprintf("http://%s:%s/login", flag.Arg(0), portRemoteServer))

	// Handle robot calls
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Hei, fai cose!!!")
		cmd := exec.Command(command, "-e", fmt.Sprintf(runCommand, "ALLARMI", phrase))
		err := cmd.Run()
		if err != nil {
			log.Println("Error during exec:", err, cmd.String())
		}
	})

	log.Fatal(http.ListenAndServe(":8080", nil))

}
