#!/bin/env python3
# encoding: utf-8

import os
import platform
import argparse
import urllib.request
from http.server import HTTPServer, BaseHTTPRequestHandler

REMOTE_PORT=8080
LOCAL_PORT=8080

class SimpleStupidThingsHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        notify('ALLARMI', 'Mamma in arrivo', 'Spegni tutto e studia')

# Use wmctrl to switch windows on Linux
def MakeHandlerClassFromArgv(init_args):
    class SwitchWindowHTTPRequestHandler(BaseHTTPRequestHandler):
        def __init__(self, *args, **kwargs):
             super(SwitchWindowHTTPRequestHandler, self).__init__(*args, **kwargs)
        def do_GET(self):
            self.send_response(200)
            self.end_headers()
            if platform.system() == 'Linux':
                os.system('wmctrl -s {}'.format(init_args))
    return SwitchWindowHTTPRequestHandler

# Use the notifier utility to send a notification
def notify(title, subtitle, message):
    t = '-title {!r}'.format(title)
    s = '-subtitle {!r}'.format(subtitle)
    m = '-message {!r}'.format(message)
    if platform.system() == 'Darwin':
        os.system('terminal-notifier {}'.format(' '.join([m, t, s])))
    if platform.system() == 'Linux':
        os.system('notify-send "{}" "{}"'.format(title,message))

# register to the Raspberry Pi server
def register(remote):
    print("registering on %s" % remote)
    urllib.request.urlopen("http://%s:%s/login" % (remote,REMOTE_PORT)).read()

# Waits notification from Raspberry Pi
def serve(switch_desktop):
    if switch_desktop is None:
        httpd = HTTPServer(('', LOCAL_PORT), SimpleStupidThingsHTTPRequestHandler)
    else:
        # if switch_desktop is set use the command "wmctrl" to switch windows
        HandlerClass = MakeHandlerClassFromArgv(switch_desktop)
        httpd = HTTPServer(('', LOCAL_PORT), HandlerClass)
    httpd.serve_forever()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-ip', help='rpi ip address', metavar='192.168.1.1')
    parser.add_argument('-sd', help='switch to different desktop', metavar='-1')
    args = parser.parse_args()

    register(args.ip)
    serve(args.sd)
