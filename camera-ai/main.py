from PIL import Image
import face_recognition
import requests
import os
import platform
import argparse
import urllib.request
from http.server import HTTPServer, BaseHTTPRequestHandler, HTTPStatus
import cgi

LOCAL_PORT=8080

def kittyAI(inpufile, outfile):
    image1 = face_recognition.load_image_file(inpufile)

    # Find all the faces in the image using the default HOG-based model.
    # This method is fairly accurate, but not as accurate as the CNN model and not GPU accelerated.
    # See also: find_faces_in_picture_cnn.py
    face_locations = face_recognition.face_locations(image1)
    print("I found {} face(s) in this photograph.".format(len(face_locations)))

    images = {}
    for face_location in face_locations:

        # Print the location of each face in this image
        top, right, bottom, left = face_location
        print("A face is located at pixel location Top: {}, Left: {}, Bottom: {}, Right: {}".format(top, left, bottom, right))
        
        height = bottom-top
        width = right-left
        filename = '{}-{}.jpg'.format(height, width)

        if os.path.exists('./{}'.format(filename)): # reuse kitty immages
            images[filename] = {'top': top, 'left': left}
        else:
            # Download kitty images
            print('download a kitty...')
            url = 'https://placekitten.com/{}/{}'.format(height, width)
            r = requests.get(url, allow_redirects=True)
            images[filename] = {'top': top, 'left': left}
            open(filename, 'wb').write(r.content)

    # Place kitty image inside the input image
    back_im = Image.open(inpufile)
    for key, value in images.items():
        im2 = Image.open(key)
        back_im.paste(im2, (value['left'], value['top']))
    back_im.save(outfile, quality=95)


class SimpleStupidThingsHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        form = cgi.FieldStorage(
            fp=self.rfile,
            headers=self.headers,
            environ={'REQUEST_METHOD': 'POST',
                     'CONTENT_TYPE': self.headers['Content-Type'],
                     })

        filename = form['uploadfile'].filename
        data = form['uploadfile'].file.read()
        path = "/tmp/{}".format(filename)

        open(path, "wb").write(data) # Save the input file
        kittyAI(path, filename)

        self.send_response(HTTPStatus.OK)
        self.end_headers()

def serve():
    httpd = HTTPServer(('', LOCAL_PORT), SimpleStupidThingsHTTPRequestHandler)
    httpd.serve_forever()


if __name__ == "__main__":
    serve()
