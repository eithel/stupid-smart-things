package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"sync"
	"time"

	"gobot.io/x/gobot"
	"gobot.io/x/gobot/drivers/gpio"
	"gobot.io/x/gobot/platforms/raspi"
)

const (
	portRemoteServer = "8080"
)

func main() {

	flag.Parse() // We want as paramenter the AI server IP

	if len(flag.Args()) < 1 {
		log.Fatalln("I need the AI server IP!!", flag.Args())
	}
	cheese, upload := make(chan bool), make(chan bool)

	// run queuePicture in a routine
	quit := make(chan struct{})
	wait := &sync.WaitGroup{}
	wait.Add(1)
	go queuePicture(flag.Arg(0), cheese, upload, quit, wait)

	// Use the raspberry with PIR Motion sensor
	r := raspi.NewAdaptor()
	sensor := gpio.NewPIRMotionDriver(r, "11", 100*time.Millisecond)

	robot := gobot.NewRobot("Doing Stuff",
		[]gobot.Connection{r},
		[]gobot.Device{sensor},
		sensorWork(sensor, cheese), // Func where robot work is defined
	)

	// We start the robot with "false", so we can control the exit
	err := robot.Start(false)
	if err != nil {
		log.Fatalf("Unable to start the robot: %v\nExit....\n", err)
	}

	// Wait the ctr-c
	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, os.Interrupt)
	<-sigint

	close(quit) // broadcast the end signal
	wait.Wait() // waits the finish of uploads

	if err := robot.Stop(); err != nil {
		log.Printf("Robot '%s', error during  shutdown: %v", robot.Name, err)
	}
}

// queuePicture takes a picture one by one
func queuePicture(server string, cheese, upload chan bool, quit chan struct{}, wait *sync.WaitGroup) {
	defer wait.Done()

	doingPhoto := false

	for {
		select {
		case <-cheese: // hey, someone say cheese! let's take a picture!
			if !doingPhoto {
				doingPhoto = true
				go takePicture(server, upload)
			}

		case <-upload: // we have finished to upload the image, so we are redy to accept another
			doingPhoto = false

		case <-quit:
			return

		}
	}
}

// sensorWork returns the function which manages the sensor events
func sensorWork(sensor gobot.Eventer, cheese chan bool) func() {
	return func() {

		// When some motion is detected, we notify the queuePicture
		sensor.On(gpio.MotionDetected, func(data interface{}) {
			log.Println(gpio.MotionDetected)
			cheese <- true

		})

		sensor.On(gpio.MotionStopped, func(data interface{}) {
			log.Println(gpio.MotionStopped)
		})

		sensor.On(gpio.Error, func(data interface{}) {
			log.Println("Some kind of error occurs:", data)
		})

	}
}

// takePicture uses raspistill to take the photo and upload to AI server
func takePicture(server string, upload chan bool) {

	filename := time.Now().Format("2006-01-02_15-04-05") + ".jpg"

	// use raspistill to take the picture
	cmd := exec.Command("raspistill", "-o", filename, "-t", "200")
	err := cmd.Run()
	if err != nil {
		log.Println("Error during exec:", err, cmd.String())
		return
	} else {
		log.Println("Cheeeeeeeeese...")
	}

	// send the picture to AI server
	err = postFile(filename, fmt.Sprintf("http://%s:%s/", server, portRemoteServer))
	if err != nil {
		log.Println("Unable to send image:", err)
	} else {
		log.Println("Image uploaded...")
	}

	upload <- true // notify sensorWork so other photos can be taken
}

// postFile makes a http POST call, with the file inside the input element "uploadfile"
func postFile(filename string, targetUrl string) error {
	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)

	// this step is very important
	fileWriter, err := bodyWriter.CreateFormFile("uploadfile", filename)
	if err != nil {
		return fmt.Errorf("error writing to buffer: %w", err)
	}

	// open file handle
	fh, err := os.Open(filename)
	if err != nil {
		return fmt.Errorf("error opening file: %w", err)
	}
	defer fh.Close()

	//iocopy
	_, err = io.Copy(fileWriter, fh)
	if err != nil {
		return fmt.Errorf("unable to copy file: %w", err)
	}

	contentType := bodyWriter.FormDataContentType()
	bodyWriter.Close()

	resp, err := http.Post(targetUrl, contentType, bodyBuf)
	if err != nil {
		return fmt.Errorf("error posting file: %w", err)
	}
	defer resp.Body.Close()
	_, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("error reading response body: %w", err)
	}
	return nil
}
