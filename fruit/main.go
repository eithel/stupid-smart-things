package main

import (
	"log"
	"os"
	"os/exec"
	"os/signal"
	"sync"
	"time"

	"gobot.io/x/gobot"
	"gobot.io/x/gobot/drivers/spi"
	"gobot.io/x/gobot/platforms/raspi"
)

func main() {

	// Create the reaspberry.
	r := raspi.NewAdaptor()

	// Create the MCP driver.
	// This driver is useful to read some analogic.
	// We set the speed to 1.35 MHz because we use 3.3V for the chip.
	// More info here: https://cdn-shop.adafruit.com/datasheets/MCP3008.pdf
	mcp := spi.NewMCP3008Driver(r, spi.WithSpeed(1350000))
	err := mcp.Start()
	if err != nil {
		log.Fatalf("Unable to start mcp chip: %v\nExit....\n", err)
	}

	audioFiles := []string{
		"flip.wav",
		"jump.wav",
		"fall.wav",
		"fireworks.wav",
		"kick.wav",
		"lifeup.wav",
		"shoot.wav",
	}

	wait := &sync.WaitGroup{}
	stopWorkers := make(chan bool) // Channel used to close workers routines.
	work := func() {
		// We run different routines to listen different MCP3008 channels.
		for k, v := range audioFiles[0:6] {
			wait.Add(1)
			go listenAndPlay(mcp, k, v, wait, stopWorkers)
		}
	}

	fruitRobot := gobot.NewRobot("Fruit Keyboard",
		[]gobot.Connection{r},
		work,
	)

	// We start the robot with "false", so we can control the exit.
	err = fruitRobot.Start(false)
	if err != nil {
		log.Fatalf("Unable to start the robot: %v\nExit....\n", err)
	}

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	// Wait the ctrl-c
	<-c

	close(stopWorkers) // Broadcast the workers
	wait.Wait()        // Wait the end of all the workers

	log.Println("quiting...")
	if err := fruitRobot.Stop(); err != nil {
		log.Printf("Robot '%s', error during  shutdown: %v\n", fruitRobot.Name, err)
	}

}

// listenAndPlay read a channel using the MCP3008 chip, and if a fruit is pressed, it plays a song.
// quit channel must be used to exit the function.
func listenAndPlay(mcp *spi.MCP3008Driver, channel int, song string, wait *sync.WaitGroup, quit chan bool) {
	oldValue := -1
	threshold := 200

	// When we exit the function, we warn.
	defer wait.Done()

	for {
		newValue, err := mcp.Read(channel)
		if err != nil {
			log.Println("Error:", err)
		} else if !(oldValue-threshold <= newValue && newValue <= oldValue+threshold) {
			if oldValue != -1 && newValue < oldValue { // Only when the button is pressed we play the song
				// We play the song in separate routine, so we don't block the button.
				go func(song string) {
					log.Printf("Playing: %s\n", song)
					cmd := exec.Command("play", "-q", song)
					err := cmd.Run()
					if err != nil {
						log.Println("Unable to play the sound:", err)
					}
				}(song)
			}
			oldValue = newValue
		}

		// Select is a blocking function. We wait data from these two channels
		select {
		case <-time.After(50 * time.Millisecond): // Wait some times.
		case <-quit: // Close the function
			return
		}
	}
}
