.PONY: help rsync-server build-server deploy-server rsync-fruit build-fruit rsync-camera build-camera

.DEFAULT_GOAL := help
SHELL := /bin/bash
current_dir = $(shell pwd)

help:
	@echo -e "$$(grep -hE '^\S+:.*##' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' -e 's/^\(.\+\):\(.*\)/\\x1b[36m\1\\x1b[m:\2/' | column -c2 -t -s :)"

build-server: ## Build motion server app for Raspberry Zero
	GOARM=6 GOARCH=arm GOOS=linux go build -o robot_server server/*.go

rsync-server: ## Rsync motion server app to rasp
	@printf "Rsync file to remote server\n"
	@rsync -hv --progress robot_server raspy0w:~

build-fruit: ## Build fruit app for Raspberry Pi 2, 3 and 4
	GOARM=7 GOARCH=arm GOOS=linux go build -o robot_fruit fruit/*.go

rsync-fruit: ## Rsync fruit app to rasp
	@printf "Rsync file to remote server\n"
	@rsync -hv --progress robot_fruit fruit/*.wav rasp3b:~

build-camera: ## Build camera app for Raspberry Zero
	GOARM=6 GOARCH=arm GOOS=linux go build -o robot_camera camera-ai/*.go

rsync-camera: ## Rsync camera app to rasp
	@printf "Rsync file to remote server\n"
	@rsync -hv --progress robot_camera rasp3b:~
