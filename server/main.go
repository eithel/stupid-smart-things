package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"

	"gobot.io/x/gobot"
	"gobot.io/x/gobot/drivers/gpio"
	"gobot.io/x/gobot/platforms/raspi"
)

const (
	serverPort = ":8080"
	clientPort = ":8080"
)

func main() {

	notify := make(chan bool) // This channel is used to notify when a movement is detected

	// Use the raspberry with PIR Motion sensor
	r := raspi.NewAdaptor()
	sensor := gpio.NewPIRMotionDriver(r, "11", 200*time.Millisecond)

	robot := gobot.NewRobot("Doing Stuff",
		[]gobot.Connection{r},
		[]gobot.Device{sensor},
		sensorWork(sensor, notify), // Func where robot work is defined
	)

	// We start the robot with "false", so we can control the exit
	err := robot.Start(false)
	if err != nil {
		log.Fatalf("Unable to start the robot: %v\nExit....\n", err)
	}

	stopManager := make(chan struct{}) // stopManager is channel used to stop the clientsManager
	register := make(chan string)      // register send the new IP to the clientsManager
	// Start the client manager
	go clientsManager(register, notify, stopManager)

	// HTTP route for register new clients
	http.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
		register <- strings.Split(r.RemoteAddr, ":")[0]
		fmt.Fprintf(w, "Hello, %q", r.RemoteAddr)
	})

	// HTTP Server
	srv := &http.Server{Addr: serverPort}
	idleConnsClosed := make(chan struct{}) //
	// Func which closes robots and http server
	go waitAndQuit(robot, srv, stopManager, idleConnsClosed)

	log.Println("Listening at", srv.Addr)

	// Start the HTTP server
	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		log.Fatalf("Unable to start HTTP server: %v\n", err)
	}

	// Wait the ctrl-c
	<-idleConnsClosed

}

// sensorWork returns the function which manages the sensor events
func sensorWork(sensor gobot.Eventer, notify chan bool) func() {
	return func() {

		// When some motion is detected, we notify the clientsManager
		sensor.On(gpio.MotionDetected, func(data interface{}) {
			log.Println(gpio.MotionDetected)
			notify <- true
		})

		sensor.On(gpio.MotionStopped, func(data interface{}) {
			log.Println(gpio.MotionStopped)
		})

		sensor.On(gpio.Error, func(data interface{}) {
			log.Println("Some kind of error occurs:", data)
		})

	}
}

// clientsManager registers new clients and notifies the clients already connected.
// If the client doesn't respond, it removes the client from the list
func clientsManager(register chan string, notify chan bool, quit chan struct{}) {

	// Map which keep all the IP registered
	listClient := make(map[string]string)
	// Chan used to send IP to be delete from the map
	deleteClient := make(chan string)

	for {
		select {
		case v := <-register:
			log.Println("Add a new client with IP:", v)
			listClient[v] = v

		case <-notify:
			for k := range listClient {
				// For each client, we lunch a routine, so we don't block the loop
				go alertClient(k, deleteClient)
				log.Println("notify: ", k)
			}

		case vd := <-deleteClient:
			delete(listClient, vd)
			log.Println("delete: ", vd)

		case <-quit:
			return

		}
	}
}

// alertClient make a simple GET request to client.
// If a error occurs, we'll delete the client
func alertClient(address string, deleteClient chan string) {
	// We suppose to run this on local network, so we keep a low timeout
	c := http.Client{Timeout: 700 * time.Millisecond}

	// We don't care about the response, check only the error
	_, err := c.Get("http://" + address + clientPort)
	if err != nil {
		log.Printf("Unable to alert %s, because: %v", address, err)
		deleteClient <- address // Send the address back to the clientsManager
	}
}

// waitAndQuit waits the ctrl-c command and then close the robot and the HTTP server draining the connections
func waitAndQuit(robot *gobot.Robot, srv *http.Server, stopManager, idleConnsClosed chan struct{}) {

	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, os.Interrupt)
	<-sigint
	log.Println("quiting...")

	if err := robot.Stop(); err != nil {
		log.Printf("Robot '%s', error during  shutdown: %v", robot.Name, err)
	}

	if err := srv.Shutdown(context.Background()); err != nil {
		log.Printf("Error during server shutdown: %v", err)
	}

	// Closing a channel is like sending something into the channel:
	// receiving statements will be received a notification
	close(stopManager)
	close(idleConnsClosed)

}
