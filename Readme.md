# LinuxDay 2019

Code and slides for **Stupid SmartThings** talk.

## Slides

Here the slides: [https://eithel.gitlab.io/stupid-smart-things](https://eithel.gitlab.io/stupid-smart-things )

## What

In this talk we are going to see a list of Smart Things created using **Raspberry Pi** board.

We are going to play with:

- Raspberry Pi 3B+ / 0 W
- Motion sensor
- Fruits
- Camera module for Raspberry Pi
- Artificial intelligence
- Cats images

# Stupid things

## Motion sensor Server-Client

Raspberry Pi is used to notify clients when any movements occur.

The client will run a local script to switch windows, so you can watch Netflix safely 😎.

Folders:

- server:
  - main.go: the server on the raspberry
- client:
  - client.py: the client on the pc

![motion sensor](slide/images/motion-in.png)

## Play music with fruits / vegetables

Raspberry Pi is used to listen any resistance variation on fruits / vegetables.

When we touch the fruits / vegetables, raspberry plays a sound!

Folder:

- fruit:
  - main.go: script for raspberry

![fruit music](slide/images/fruit-in.png)

## Motion Sensor and AI: fun images

Raspberry Pi is used take a picture when any movements occur.

We upload the pictures inside an "AI server" which detects faces inside pictures.

If some faces has been found, we replace them with some kitty images.

Folders:

- camera-ai:
  - main.py: the AI server
  - main.go: raspberry client

![kitty AI](slide/images/kitty-in.png)
