# Instruction

Inside this folder run:

`docker build . -t revealjs:3.8.0`

After the image creation, run:

`mkdir node_modules`

`docker run --rm -v $(pwd)/node_modules:/html/node_modules revealjs:3.8.0 npm install`

wait ....

Then you can run the server using:

```
docker run -d --name slide_linux_day_2019 \
    -v $(pwd)/node_modules:/html/node_modules \
    -v $(pwd)/images:/html/images \
    -v $(pwd)/slides.md:/html/slides.md \
    -v $(pwd)/index.html:/html/index.html \
    -p 8000:8000 revealjs:3.8.0 npm start
```
