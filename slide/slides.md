## Stupid Smart Things

Cristian Pavan | Stefano Morandi
<!-- .element style="margin-top:100px; font-size:80%;" -->

Linux Day 2019-10-26
<!-- .element style="font-size:60%;" -->

**PNLUG, Linuxday 2019**
<!-- .element style="margin-top:50px; font-size:60%;" -->

---

## Hello !
I'm Cristian and I'm Developer ([@tux_eithel](https://twitter.com/tux_eithel)).  
I'm Stefano and I'm DevOps ([@umorando](https://twitter.com/uomorando)).  
We work at [AKQA](https://www.akqa.com/).

And we ❤️ Linux! <!-- .element: class="fragment" -->

and play with Raspberry Pi 🔌 💡 🔨 💻 ... <!-- .element: class="fragment" -->

---

Today we will play a little bit

with the Raspberry Pi

to create SmartThings!

---

## SmartThings

A SmartThing is a computing device that do some actions after events has been detected.

It can communicate with other devices. <!-- .element: class="fragment" -->

We associate them to the concept of **Internet of Things**.<!-- .element: class="rasp-green" -->
<!-- .element: class="fragment" -->

notes: example the thermostats<br/><br/>lower the blinds automatically when it's raining outside Difference with IoT<br/><br/>concept of "smart building" 

+++

Example of SmartThings devices are:

- sensors
- cams
- bulbs
- outlets
- buttons
- thermostats
- ...

---

Generally these devices are very closed, and we don't have the full control.

They are also expensive.

So today we are going to build **our** SmartThing devices!<!-- .element: class="rasp-green" -->
<!-- .element: class="fragment" -->

notes: we need a device, so we introduce the Raspberry

---

## Raspberry Pi

It is a single computer board developed by the Raspberry Pi Foundation to 
promote teaching of basic computer science in schools and in developing countries. <!-- .element: class="fragment" -->

![raspberry logo](images/pi-logo.png) <!-- .element: class="fused fragment" height="20%" width="20%" -->

+++

There are different boards versions.  
The latest released one is really dope!

+++

<!-- .slide: class="two-column" -->

![Rasberry Pi 4](images/board_pi4.png) <!-- .element class="fused" -->
<!-- .element class="col" -->

* 1-4 GB of RAM
* 4×Cortex-A72 1.5 GHz
* 1 Gbit Ethernet
* WiFi ac
* Bluetooth 5
* 2 x USB 3
* 2 x micro-HDMI
* USB C (for power supply)
* 17 GPIO for manage low-level peripherals !
* Cost between 45 - 65 €
  
<!-- .element class="col small-text"  -->

notes: Brief explanation of other models: some model doesn't have ethernet, wi-fi, less ram

+++

<!-- .slide: class="two-column" -->

![Rasberry 0 W](images/board_pi0w.png) <!-- .element class="fused" -->
<!-- .element class="col" -->

* Single Core ARM11 1 GHz
* 512 MB of RAM
* mini-HDMI
* mini-USB
* WiFi
* Bluetooth
* 17 GPIO
* Cost between 20 - 25 €

<!-- .element class="col small-text"  -->

---

## Why use a Raspberry Pi

**Low cost:**<!-- .element:  class="rasp-green" -->
Makes it easy to get one to play.
<!-- .element: class="fragment color-none" -->

**It's like your pc, but smaller:** <!-- .element:  class="rasp-green" -->  
you can connect your monitor, keyboard and mouse.  
<!-- .element: class="fragment color-none" -->
It has WiFi and Bluetooth.  
<!-- .element: class="fragment" -->
It runs a **full OS** <!-- .element: class="rasp-green" -->
<!-- .element: class="fragment" -->

notes: It has Raspbian, a _Debian_ version for Raspberry Pi.<br/><br/>Large codebase.<br/><br/>It also supports other Operating Systems: Ubuntu (Core, MATE and Server), Windows 10 IoT Core. 

+++

### Resources

A lot of projects have been made using Raspberry Pi: there are a lot of **docs and videos.** <!-- .element: class="rasp-green" -->

Easy to find the right sensor.

+++

### You don't need to be an electronic engineer

⚡️⚡️⚡️⚡️⚡️⚡️

notes: sensors are easy to plug, resource are wonderful<br/><br/>Talk about our experiences.

---

## Let's build something

note: to build somethings, we need to solve real problem. make some jokes :)<br/><br/>Air quality, water plants, temp stations

+++

### Problem

I'm _"studying"_ at PC and mum enters in the room.

I need to switch my PC window. <!-- .element: class="fragment" -->

Let's use a SmartThings to do something stupid 😁 ! <!-- .element: class="fragment" -->

notes: here the stupid thing!

+++

### We need

**A Raspberry Pi** <!-- .element: class="fragment rasp-green" -->

We are building our SmartThing using it. <!-- .element: class="fragment small-text" -->

**A Sensor** <!-- .element: class="fragment rasp-green" -->

To detect the mum inside our room. <!-- .element: class="fragment small-text" -->

**Some software** <!-- .element: class="fragment rasp-green"-->

To let sensor communicate with other "things". <!-- .element: class="fragment small-text" -->

notes: Of course a computer is also for build things outside the Raspberry.<br/>Some software because otherwise we build "another sensor".

---

### The sensor

We are going to use a **Motion Sensor**.

These kind of sensor emits a signal when some amounts of movement has been detected.

+++

We are going to use the sensor _HC-SR501_.

![scheme hc-sr501](images/scheme-hc-sr501.png)

notes: jump, potenziometri, only 3 pin

---

### The software

We use Gobot [https://gobot.io/](https://gobot.io/).

It is a robotics/IoT framework written in Go.

![Go Logo](images/go-logo.png)  <!-- .element class="fused"  height="20%" width="20%"  -->

It supports 35 platforms, abstracting sensors and platforms.

notes: easy to switch between sensors and platforms.

+++

Due the nature of Go:
- it's easy write some code in one architecture and cross compile to another:
  - `GOARM=6 GOARCH=arm GOOS=linux go build -o *.go`
- it's easy to create endpoints.
- it's easy to do concurrency.

---

### Architecture

A simple program in Go which uses the sensor to detect motions. <!-- .element: class="fragment" -->

It acts as **server** on the Raspberry Pi.<!-- .element: class="rasp-green" -->  
It allows **clients** to be registered.<!-- .element: class="rasp-green" -->
<!-- .element: class="fragment" -->

Each time a motion is detect, it notifies the _clients_.
<!-- .element: class="fragment" -->

All communications are made using http, so it's very easy to implement and consume. <!-- .element: class="fragment" -->

No authentication, don't run in production!! <!-- .element: class="fragment" -->

notes: the client is independent <br/>separation client-server

---

## Demo Time

![fruit image](images/motion.png) <!-- .element class="fused"  -->

notes: let do the actual stuff

---

## Let's build something - 2

---

### Let's play some music

We try to use some fruits / vegetables to **play** some music!<!-- .element: class="rasp-green" -->
<!-- .element: class="fragment" -->

+++

### We need

Raspberry Pi connected with some audio device.

A chip to "detect" analogous variations. <!-- .element: class="fragment" -->

A basic script to listen these variations and play sounds. <!-- .element: class="fragment" -->

Some fruits / vegetables. <!-- .element: class="fragment" -->

notes: explain rasp has only digital input.

---

### The chip 

We are going to use the **MCP3008** chip which converts analogue signals in digital and use SPI ports on Raspberry Pi.

![mcp3008](images/mcp3008.png) <!-- .element class="fused"  height="40%" width="40%"  -->

notes: explain the number of port and how easy is to connect with rasp.<br/>You have to enable SPI on rasp

---

### How it works

It's based on the principle that **all the things are conductive,** that means in this case that when
toughing the fruits / vegetables we alter their resistance<!-- .element: class="rasp-green" -->

It is inspired by [Makey Makey](https://makeymakey.com/). 
<!-- .element: class="fragment" -->

---

### The software

We've made a simple script which polls MCP3008 channels. <!-- .element: class="fragment" -->

In case any value changes, it means that someone has touched the fruit / vegetable. <!-- .element: class="fragment" -->

We link each input channel to a different sound. <!-- .element: class="fragment" -->

---

### Demo time

![fruit image](images/fruit.png) <!-- .element class="fused"  -->

notes: let do the actual stuff

---

## Let's build something - 3

---

### Make some funny photos

Anytime there's some movement detected, we take a picture!

We then use **AI** to detect faces and replace them with kitty images, making those prettier 😻. <!-- .element: class="rasp-green" -->
<!-- .element: class="fragment" -->

---

### We need

Raspberry Pi with camera module.

A server where AI is used to detect faces whenever an images is uploaded. <!-- .element: class="fragment" -->

notes: explain camera for rasp and other mods 

---

### The software

We use a python library - **Face Recognition** - to detect and manipulate faces.

It provides a model with an accuracy of 99.38%. <!-- .element: class="fragment" -->

Once a face has been recognized, we call an external service [https://placekitten.com](https://placekitten.com) 
to generate the 🐱 image to swap with the face. 
<!-- .element: class="fragment" -->

notes: it has a model, it can use a GPU (more speed)

---

### Architecture

We wrote a simple program in Go which uses the sensor to detect motions. <!-- .element: class="fragment" -->

When a motion has been detect, we use **raspistill** utility to take a picture.<!-- .element: class="rasp-green" --> 
<!-- .element: class="fragment" -->

We upload the image into the AI "server":  
It generates the cat-images. <!-- .element: class="fragment" -->

note: it could send an email

---

### Demo time

![kitty image](images/kitty.png) <!-- .element class="fused"  -->

notes: let do the actual stuff

---

### Final thoughts

---

### IoT

Connecting it to the internet: becomes a real IoT device.

You can control your devices from all around the world. <!-- .element: class="fragment" -->

Opening your device to internet can be scary 😟. <!-- .element: class="fragment" -->

It requires resources to maintain the system updated. <!-- .element: class="fragment" -->

notes: authentication, updates, api version, dependencies from other software.

---

### If you need more power...

Use cloud computing to get more CPU power.

Useful when your device it's really small. <!-- .element: class="fragment" -->

You're going to use a closed solution. <!-- .element: class="fragment" -->

---

### AI

Use artificial intelligence to do more awesome stuff!!

You can make your device more fun! <!-- .element: class="fragment" -->

... or precise. <!-- .element: class="fragment" -->

It's hard to use open source solutions. <!-- .element: class="fragment" -->

---

## Thank you!

Now it's your turn to do stupid SmartThings!

Cristian ([@tux_eithel](https://twitter.com/tux_eithel))  
Stefano ([@umorando](https://twitter.com/uomorando))
<!-- .element style="text-align: left; margin-top:100px; font-size:80%;" -->

Linux Day 26-10-2019
<!-- .element style="text-align: left; margin-top:50px; font-size:60%;" -->
